package com.proses.html;

import com.proses.allfunc.ValueJSON;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

public class TableHTML {

    private String[] idh, idh1;
    private String headerField = "", bodyValue = "";
    private ValueJSON keyHeader;
    private String JA = "";
    private String[] stringArray;
    private int size;
    private JSONObject json;
    private JSONArray jsonArray;
    
    public TableHTML(String strJA){
        JA = strJA;
    }

    public String headerTable(){
    	keyHeader = new ValueJSON(JA);
        idh = keyHeader.getKey().split(",");
        jsonArray = new JSONArray(JA);
        headerField += "<thead>";
        headerField += "<tr>";
        Arrays.sort(idh);
        headerField += "<th>";
        headerField += "#";
        headerField += "</th>";
        for (int i = 0; i < 1; i++) {

            json = jsonArray.getJSONObject(i);
            for (String s : idh) {
                assert s != null;
                if(json.optString(s) == null | json.optString(s).equals(""))continue;
                headerField += "<th>";
                headerField += json.optString(s);
                headerField += "</th>";

            }
        }

        headerField += "</tr>";
        headerField += "</thead>";

        return headerField;
    }

    public String valueTable(){
    	keyHeader = new ValueJSON(JA);
        idh1 = keyHeader.getKey().split(",");
        Arrays.sort(idh1);
        jsonArray = new JSONArray(JA);
    	size = jsonArray.length();
        bodyValue += "<tbody>";
        int b= 1;
        for (int i = 1; i < size; i++) {

            json = jsonArray.getJSONObject(i);
            bodyValue += "<tr>";
            bodyValue += "<td>";
            bodyValue += b++;
            bodyValue += "</td>";
            for (String a : idh)
            {
                if(json.optString(a) == null | json.optString(a).equals(""))continue;

                bodyValue += "<td>";
                bodyValue += json.optString(a);
                bodyValue += "</td>";
            }

            bodyValue += "</tr>";

        }
        bodyValue += "</tbody>";
    	return bodyValue;
    }
}
