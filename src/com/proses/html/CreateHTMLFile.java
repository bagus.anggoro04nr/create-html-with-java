package com.proses.html;

public class CreateHTMLFile {
	
	private String HeaderTable = "", BodyTable = "", convertHTML = "";

    public CreateHTMLFile(String header, String body){
    	HeaderTable = header;
    	BodyTable = body;
    }

    public String convertToHTML() {
    	
    	convertHTML += "<html>";
    	convertHTML += "<head>";
    	convertHTML += "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.11.2/css/all.css\">";
		convertHTML += "<link rel=\"stylesheet\" href=\"plugins/css/bootstrap.min.css\">";
		convertHTML += "<link rel=\"stylesheet\" href=\"plugins/css/mdb.min.css\">";
		convertHTML += "<link rel=\"stylesheet\" href=\"plugins/css/style.css\">";
		convertHTML += "<script type=\"text/javascript\" src=\"plugins/js/jquery.min.js\"></script>";
		convertHTML += "<script type=\"text/javascript\" src=\"plugins/js/popper.min.js\"></script>";
		convertHTML += "<script type=\"text/javascript\" src=\"plugins/js/bootstrap.min.js\"></script>";
		convertHTML += "<script type=\"text/javascript\" src=\"plugins/js/mdb.min.js\"></script>";
    	convertHTML += "</head>";
    	convertHTML += "<body>";
		convertHTML += "<div class=\"table-responsive\">";
    	convertHTML += "<table class=\"table table-striped w-auto\">";
    	convertHTML += HeaderTable;
    	convertHTML += BodyTable;
    	convertHTML += "</table>";
		convertHTML += "</div>";
    	convertHTML += "</body>";
    	convertHTML += "<html>";
    	
    	return convertHTML;
    }
}
