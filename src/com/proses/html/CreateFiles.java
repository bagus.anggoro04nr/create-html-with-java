/**
 * 
 */
package com.proses.html;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Pangeran_Jack_4Nr
 *
 */
public class CreateFiles {
	
	private File file;
	private FileWriter fw;
	private BufferedWriter bw;
	private String fileHTML, path, pathFile;
	private URI uri;
	private Desktop desktop;

	public CreateFiles(String dataHTML) {
		fileHTML = dataHTML;
	}
	
	public void toFiles() throws IOException, URISyntaxException {
		path = System.getProperty("user.dir");
		pathFile = path + "\\output\\report.html";
		file = new File(pathFile);
		if(!file.getParentFile().exists()){
			file.getParentFile().mkdirs();
		}
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		fw = new FileWriter(file);
        bw = new BufferedWriter(fw);
        bw.write(fileHTML);
        bw.flush();
        bw.close();
		openInBrowser();
	}

	private void openInBrowser() throws IOException, URISyntaxException {
		uri = new URL("file", null, "//" + pathFile.replace("\\", "/")).toURI();
		desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE))
			desktop.browse(uri);
	}

}
