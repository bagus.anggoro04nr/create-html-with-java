/**
 * 
 */
package com.proses.allfunc;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Pangeran_Jack_4NR
 *
 */
class ToArrays {

	private static String[] stringArray;
	private static JSONArray jsonArray;
	private int a = 0;
	private static JSONObject json;
	private ObjectMapper mapper;

	ToArrays(JSONArray JsArr) {
		jsonArray = JsArr;
		
	}
	
	String[] ConvertArr(String[] idKey) {
		mapper = new ObjectMapper();
		for (String s : idKey) {

			for (int i = 0; i < jsonArray.length(); i++) {
				json = jsonArray.getJSONObject(i);
				if (json.keySet().contains(s)) {
					if (a > 0) stringArray[a] += ",";
					if (i > 0) stringArray[a] += ",";
					if(json.get(s).equals(""))continue;
						stringArray[a] += (String) json.get(s);
				}
			}

			a++;
		}
		return stringArray;
	}

}
