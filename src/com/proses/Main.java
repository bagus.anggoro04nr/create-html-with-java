package com.proses;

import com.proses.html.CreateFiles;
import com.proses.html.CreateHTMLFile;
import com.proses.html.TableHTML;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
	
	private static TableHTML th;
	private static CreateHTMLFile chf;
	

    public static void main(String[] args) throws IOException, URISyntaxException {

    	th = new TableHTML(args[0]);
    	chf = new CreateHTMLFile(th.headerTable(), th.valueTable());
    	new CreateFiles(chf.convertToHTML()).toFiles();
		
    }
}
